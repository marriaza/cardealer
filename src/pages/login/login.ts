import { Component } from '@angular/core';
import {HomePage} from "../home/home";
import { SetpassPage } from '../setpass/setpass';

import { IonicPage, NavController, NavParams,ToastController,LoadingController } from 'ionic-angular';
import { ExxisProvider } from '../../providers/exxis/exxis';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  usuario={
    usuario:"marriaza",
    pass:"mau"
  }


  constructor(public navCtrl: NavController, public navParams: NavParams,public exxis:ExxisProvider,public toast: ToastController,public cargador:LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  
  login() {
    this.mostrar_cargador(1);

    this.exxis.login(this.usuario)
      .then(
      (res) => { 
           this.ocultar_cargador();
           let datos_us=res;

           console.log(datos_us);
           if (!datos_us){
                this.mostrarToast("Usuario no encontrado, verifique sus credenciales de ingreso");               

              }else if(datos_us["estado"]==400){
                this.mostrarToast("Usuario no encontrado, verifique sus credenciales de ingreso");
                
              }else if(datos_us["estado"]==600){
                this.mostrarToast("Existe un problema en la respuesta del servidor "+datos_us["error"]);                
              }
              else{    
                if((datos_us[0].U_CD_UsrPswd=="")||(datos_us[0].U_CD_UsrPswd ===null)) {
                  this.navCtrl.setRoot(SetpassPage,{usr:datos_us[0]});
                }else{
                  this.navCtrl.setRoot(HomePage,{usr:datos_us[0]});
                }         
                
              }
          //this.navCtrl.setRoot(HomePage,datos_us);
        },
        (error) =>{
          this.ocultar_cargador();
          this.mostrarToast("Existe un problema en la respuesta del servidor");
          console.error(error);
        }
      )
    }


  mostrarToast($mensaje){
    let toast = this.toast.create({
      message: $mensaje,
      duration: 3000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();   
    }
    private loader = this.cargador.create({
      content: "Por favor aguarde un momento ...",
      
    });
    mostrar_cargador($accion){
      if($accion==1)
        this.loader.present();
      else
        this.loader.dismiss();
    }
    ocultar_cargador(){
      this.mostrar_cargador(0);
      this.loader = this.cargador.create({
        content: "Por favor aguarde un momento ...",                  
      });
    }
    forgotPass(){
      console.log("olvido su contraseña");
    }
}
