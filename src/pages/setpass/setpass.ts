import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,LoadingController } from 'ionic-angular';
import { ExxisProvider } from '../../providers/exxis/exxis';
import {HomePage} from "../home/home";
/**
 * Generated class for the SetpassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-setpass',
  templateUrl: 'setpass.html',
})
export class SetpassPage {
	empleado={};
  constructor(public navCtrl: NavController, public navParams: NavParams
  ,public exxis:ExxisProvider
  ,public toast: ToastController
  ,public cargador:LoadingController
  ) {
  	this.empleado=this.navParams.get("usr");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SetpassPage');
  }
  	actualizarClave() {
	
  		if(this.empleado["clave"]!=this.empleado["clave2"]){
  			this.mostrarToast("Los Datos ingresados no coinciden asegurese de registrar la misma clave");
  		}else{
  			this.mostrar_cargador(1);
  			this.exxis.act_du(this.empleado)
		      .then(
		      (res) => { 
		           this.ocultar_cargador();
		           let datos_us=res;
		           if (datos_us["estado"]==200)		           
		          	this.navCtrl.setRoot(HomePage,{usr:this.empleado});
		          	else
		          	this.mostrarToast("Existe un problema en la respuesta del servidor");
		        },
		        (error) =>{
		          this.ocultar_cargador();
		          this.mostrarToast("Existe un problema en la respuesta del servidor");
		          console.error(error);
		        }
		      )

  		}
	    

	    
    }


  mostrarToast($mensaje){
    let toast = this.toast.create({
      message: $mensaje,
      duration: 3000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();   
    }
    private loader = this.cargador.create({
      content: "Por favor aguarde un momento ...",
      
    });
    mostrar_cargador($accion){
      if($accion==1)
        this.loader.present();
      else
        this.loader.dismiss();
    }
    ocultar_cargador(){
      this.mostrar_cargador(0);
      this.loader = this.cargador.create({
        content: "Por favor aguarde un momento ...",                  
      });
    }
}
