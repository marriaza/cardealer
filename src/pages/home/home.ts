import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {AgendamientoPage} from '../agendamiento/agendamiento';
import {RecepcionlPage} from '../recepcionl/recepcionl';
import {ReportelPage} from '../reportel/reportel';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  empleado={
  ASIGNACION:0,
  RECEPCION:0,
  REGISTRO:0,
  TOTAL:0
  };
  
  asignacion=0;
  recepcion=0;
  registro=0;

  constructor(public navCtrl: NavController,public navParams: NavParams) {
    this.empleado=this.navParams.get("usr");
    if((this.empleado.ASIGNACION==1))
      this.asignacion=1;
    else
      this.asignacion=0;
    if((this.empleado.RECEPCION==1))
      this.recepcion=1;
    else
      this.recepcion=0;
    if((this.empleado.REGISTRO==1)||(this.empleado.TOTAL==1))
      this.registro=1;
    else
      this.registro=0;


  }
  irAgendamiento() {
    this.navCtrl.push(AgendamientoPage);
  }
  irRecepcion() {
    this.navCtrl.push(RecepcionlPage,{usr:this.empleado});
  }
  irReporte() {
    this.navCtrl.push(ReportelPage);
  }
  
}
