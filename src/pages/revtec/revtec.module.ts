import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RevtecPage } from './revtec';

@NgModule({
  declarations: [
    RevtecPage,
  ],
  imports: [
    IonicPageModule.forChild(RevtecPage),
  ],
})
export class RevtecPageModule {}
