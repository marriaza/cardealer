import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
/**
 * Generated class for the RevtecPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-revtec',
  templateUrl: 'revtec.html',
})
export class RevtecPage {
  image: string = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,private camera: Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RevtecPage');
  }
  options: CameraOptions = {
    quality: 20,
    encodingType: this.camera.EncodingType.JPEG,
    destinationType: this.camera.DestinationType.DATA_URL,
    saveToPhotoAlbum: true,
    targetWidth:800,
    targetHeight:800,
    correctOrientation:true
  }
  tomarfoto(){
    this.camera.getPicture(this.options)
    .then(imageData => {
      this.image = 'data:image/jpeg;base64,' + imageData;

       })
   }


}
