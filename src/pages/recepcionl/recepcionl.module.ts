import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecepcionlPage } from './recepcionl';

@NgModule({
  declarations: [
    RecepcionlPage,
  ],
  imports: [
    IonicPageModule.forChild(RecepcionlPage),
  ],
})
export class RecepcionlPageModule {}
