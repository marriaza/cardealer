import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import {RecepcionPage} from '../recepcion/recepcion';
import { ExxisProvider } from '../../providers/exxis/exxis';
/**
 * Generated class for the RecepcionlPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recepcionl',
  templateUrl: 'recepcionl.html',
})
export class RecepcionlPage {

  public agendados=[];
  agendadosx=[];
  empleado={};
  palabra="";

  constructor(public navCtrl: NavController, public navParams: NavParams
  ,public cargador:LoadingController
  ,public exxis:ExxisProvider
  ) {
  this.empleado=this.navParams.get("usr");
  this.obtenerOTs();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecepcionlPage');
  }
 
  private loader = this.cargador.create({
      content: "Por favor aguarde un momento ...",      
    });

 obtenerOTs() {
    this.mostrar_cargador(1);
    this.exxis.obtenerOTs()
      .then(
      (res) => { 
           this.ocultar_cargador();
           this.agendados=res;
           this.agendadosx= this.agendados;        
        },
        (error) =>{
          this.ocultar_cargador();
          console.log(error);
        }
      )
    }

    mostrar_cargador($accion){
      if($accion==1)
        this.loader.present();
      else
        this.loader.dismiss();
    }
    ocultar_cargador(){
      this.mostrar_cargador(0);
      this.loader = this.cargador.create({
        content: "Por favor aguarde un momento ...",                  
      });
    }
    buscar(){
      let nvect=[];
      if (this.palabra.length > 0) {
          this.agendados=this.agendadosx;

          this.palabra = this.palabra.toLowerCase();
          this.agendados.forEach(item => {
            
            //let propValueList = Object.values(item);
            let propValueList =Object.keys(item).map(key => item[key]);
            for(let i=0;i<propValueList.length;i++)
                {
                  if (propValueList[i]) {
                    if (propValueList[i].toString().toLowerCase().indexOf(this.palabra) > -1)
                    {
                      nvect.push(item);
                      break;
                    }
                  }
            } 

          });
          this.agendados=nvect;
         }else{
          this.agendados=this.agendadosx;
         }
      }
       
    
  irDetalleRecepcion(ot) {
  ot.usr=this.empleado;
    this.navCtrl.push(RecepcionPage,ot);
  }
}
