import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReportelPage } from './reportel';

@NgModule({
  declarations: [
    ReportelPage,
  ],
  imports: [
    IonicPageModule.forChild(ReportelPage),
  ],
})
export class ReportelPageModule {}
