import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgendamientoPage } from './agendamiento';

@NgModule({
  declarations: [
    AgendamientoPage,
  ],
  imports: [
    IonicPageModule.forChild(AgendamientoPage),
  ],
})
export class AgendamientoPageModule {}
