import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,LoadingController } from 'ionic-angular';

import {RevtecPage} from '../revtec/revtec';
import {FirmaPage} from '../firma/firma';
import { ExxisProvider } from '../../providers/exxis/exxis';
/**
 * Generated class for the RecepcionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recepcion',
  templateUrl: 'recepcion.html',
})
export class RecepcionPage {
  ot={};
  fecha="";
  km=0;
  descripcion="";
  comentario="";
  pertenencias=[];
  pertenecias=[];
  xpertenencias=[];

  constructor(public navCtrl: NavController, public navParams: NavParams
  ,public exxis:ExxisProvider
  ,public toast: ToastController
  ,public cargador:LoadingController
  ) {
    this.ot=navParams.data;
    this.km=parseInt(this.ot["kilometraje"]);
    this.fecha= new Date().toISOString();

    this.obtenerPertenencias();


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecepcionPage');
  }
  
 armarPertenencias(){
      
    for (var i = 0; i < this.pertenencias.length; i++) {
        this.pertenencias[i].presente="N";
        for (var j = 0; j < this.xpertenencias.length; j++) {                  
           if(this.pertenencias[i].Code==this.xpertenencias[j].Code)
              this.pertenencias[i].presente="Y";
        }
        
    }

  }
  
  irRevTec(){
    this.navCtrl.push(RevtecPage);
  }
  
  irFirma(){
    this.navCtrl.push(FirmaPage);
  }

  obtenerPertenencias() {
      this.mostrar_cargador(1);
      this.exxis.obtenerPertenencias()
        .then(
        (res) => { 
             this.ocultar_cargador();
             this.pertenencias=res;                 

          },
          (error) =>{
            this.ocultar_cargador();
            console.log(error);
          }
        )
  }


  almacenar() {  
        
        this.armarPertenencias();
        //console.log(this.pertenencias);
        this.ot["pertenencias"]=this.pertenencias;
        this.ot["km"]=this.km;
        this.ot["descripcion"]=this.descripcion;
        this.ot["comentario"]=this.comentario;
        this.exxis.act_ot(this.ot)
          .then(
          (res) => { 
               this.ocultar_cargador();
               let datos_us=res;
               if (datos_us["estado"]==200)               
                this.mostrarToast("Informacion Actualizada");
                else
                this.mostrarToast("Existe un problema en la respuesta del servidor");
            },
            (error) =>{
              this.ocultar_cargador();
              this.mostrarToast("Existe un problema en la respuesta del servidor");
              console.error(error);
            }
          ) 
           
  }

    mostrarToast($mensaje){
        let toast = this.toast.create({
          message: $mensaje,
          duration: 3000,
          position: 'top'
        });
      
        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });
  
        toast.present();   
    }
    
    private loader = this.cargador.create({
      content: "Por favor aguarde un momento ...",
      
    });

    mostrar_cargador($accion){
      if($accion==1)
        this.loader.present();
      else
        this.loader.dismiss();
    }

    ocultar_cargador(){
      this.mostrar_cargador(0);
      this.loader = this.cargador.create({
        content: "Por favor aguarde un momento ...",                  
      });
    }

}
