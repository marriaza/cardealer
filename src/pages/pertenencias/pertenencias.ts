import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ExxisProvider } from '../../providers/exxis/exxis';
/**
 * Generated class for the PertenenciasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pertenencias',
  templateUrl: 'pertenencias.html',
})
export class PertenenciasPage {

  constructor(public navCtrl: NavController, public navParams: NavParams
   	,public cargador:LoadingController
  	,public exxis:ExxisProvider
  ) {
	
	
  }

	  ionViewDidLoad() {
	    console.log('ionViewDidLoad PertenenciasPage');
	  }



}
