import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PertenenciasPage } from './pertenencias';

@NgModule({
  declarations: [
    PertenenciasPage,
  ],
  imports: [
    IonicPageModule.forChild(PertenenciasPage),
  ],
})
export class PertenenciasPageModule {}
