import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Camera} from '@ionic-native/camera';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { LogoutPage } from '../pages/logout/logout';
import { AgendamientoPage } from '../pages/agendamiento/agendamiento';
import { RecepcionlPage } from '../pages/recepcionl/recepcionl';
import { RecepcionPage } from '../pages/recepcion/recepcion';
import { ReportePage } from '../pages/reporte/reporte';
import { ReportelPage } from '../pages/reportel/reportel';

import { RevtecPage } from '../pages/revtec/revtec';
import { FirmaPage } from '../pages/firma/firma';
import { SetpassPage } from '../pages/setpass/setpass';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { File } from '@ionic-native/file';
import { Draw } from '../components/draw/draw';
import { SignComponent } from '../components/sign/sign';
import { ExxisProvider } from '../providers/exxis/exxis';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    LogoutPage,
    AgendamientoPage,
    RecepcionlPage,
    RecepcionPage,
    ReportePage,
    ReportelPage,
    
    RevtecPage,
    FirmaPage,
    SetpassPage,
    Draw,
    SignComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    LogoutPage,
    AgendamientoPage,
    RecepcionlPage,
    RecepcionPage,
    ReportePage,
    ReportelPage,
   
    RevtecPage,
    FirmaPage,
    SetpassPage
  ],
  providers: [
    StatusBar,
    SplashScreen,  
    Camera, 
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    File,    
    ExxisProvider
  
  ]
})
export class AppModule {}
