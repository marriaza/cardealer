import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
/*
  Generated class for the ExxisProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ExxisProvider {

  public url = 'http://localhost:8080/exxis/cardealermovil/modelo.php'; 
  public options = {
    headers: {      
      'Content-Type': 'application/x-www-form-urlencoded',
    }
  }; 
  public pertenencias=[];

  constructor(public http: HttpClient) {
    console.log('Hello ExxisProvider Provider');
  }

  login($usr){
    let datos ={accion:1,usr:$usr.usuario, pswd:$usr.pass};       
      return new Promise(resolve => {
          this.http.post(this.url,datos,this.options)
          .subscribe(data => {
              resolve(data);
            });
      });
  }

  obtenerOTs(){
     let datos ={accion:2};       
        return new Promise<any>(resolve => {
            this.http.post(this.url,datos,this.options)
            .subscribe(data => {
                resolve(data);
              });
        });
  }
  obtenerPertenencias(){
     let datos ={accion:3};       
        return new Promise<any>(resolve => {
            this.http.post(this.url,datos,this.options)
            .subscribe(data => {
                resolve(data);
              });
        });
  }
 act_du($usr){
    let datos ={accion:4,usr:$usr};       
      return new Promise(resolve => {
          this.http.post(this.url,datos,this.options)
          .subscribe(data => {
              resolve(data);
            });
      });
  }
act_ot($ot){
    let datos ={accion:5,ord:$ot};       
      return new Promise(resolve => {
          this.http.post(this.url,datos,this.options)
          .subscribe(data => {
              resolve(data);
            });
      });
  }
 
 


}
