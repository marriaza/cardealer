import { Component,ViewChild, Renderer } from '@angular/core';
import { Platform,normalizeURL } from 'ionic-angular';
import { File, IWriteOptions } from '@ionic-native/file';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the DrawComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

const STORAGE_KEY = 'IMAGE_LIST';

@Component({
  selector: 'draw',
  templateUrl: 'draw.html'
})
export class Draw {

  @ViewChild('myCanvas') canvas: any;
 
  canvasElement: any;
  lastX: number;
  lastY: number;

  currentColour: string = '#006699';
  availableColours: any;

  brushSize: number = 10;
  img_url="";

  storedImages = [];
  img = new Image();
  

  constructor(public platform: Platform, public renderer: Renderer, private file: File, private storage: Storage) {
    this.availableColours = [
      '#006699',     
      '#e74c3c',
      '#000000'
    ];
   
    this.img.src = "../assets/imgs/vehiculo.png";   
  }
  

  ngAfterViewInit(){    
     
    this.canvasElement = this.canvas.nativeElement;
    this.canvasElement.width =this.platform.width() + '';
    this.canvasElement.height = this.platform.height()/3 + '';
    
  }
  ngAfterViewChecked(){
    this.canvasElement.getContext('2d').drawImage(this.img,0, 0,this.platform.width()-15,this.canvasElement.height);
  }



  changeColour(colour){
      this.currentColour = colour;
  }

  changeSize(size){
      this.brushSize = size;
  }

  handleStart(ev){
      
      var canvasPosition = this.canvasElement.getBoundingClientRect();
      this.lastX = ev.touches[0].pageX - canvasPosition.x;
      this.lastY = ev.touches[0].pageY- canvasPosition.y;
      
  }

  handleMove(ev){ 
      var canvasPosition = this.canvasElement.getBoundingClientRect();     
      let ctx = this.canvasElement.getContext('2d');      
      
      let currentX = ev.touches[0].pageX - canvasPosition.x;
      let currentY = ev.touches[0].pageY - canvasPosition.y;       
      ctx.lineJoin = 'round';
      ctx.strokeStyle = this.currentColour;
      ctx.lineWidth = 5;       
      ctx.beginPath();
      ctx.moveTo(this.lastX, this.lastY);
      ctx.lineTo(currentX, currentY);
      ctx.closePath();
      ctx.strokeStyle = this.currentColour;
      ctx.lineWidth = this.brushSize;
      ctx.stroke();       
      this.lastX = currentX;
      this.lastY = currentY;
  }

  clearCanvas(){
      let ctx = this.canvasElement.getContext('2d');
      ctx.clearRect(0, 0, this.canvasElement.width, this.canvasElement.height);  
  }

// codigo para guardar la imagen


  saveCanvasImage() {
    var dataUrl = this.canvasElement.toDataURL();
    console.log('data: ', dataUrl);
    let ctx = this.canvasElement.getContext('2d');
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears the canvas
   
    let name = new Date().getTime() + '.png';
    let path =  this.file.externalRootDirectory+'Pictures/';
    path = normalizeURL(path);
    let options: IWriteOptions = { replace: true };   
    var data = dataUrl.split(',')[1];    
    let blob = this.b64toBlob(data, 'image/png');    
    this.file.writeFile(path, name, blob,options).then(res => {
      //this.storeImage(name);
      //alert(res);
      alert(path);
    }, err => {
      alert(err);
    });
  }
  b64toBlob(b64Data, contentType) {
    
    contentType = contentType || '';
    var sliceSize = 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];
   
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);
   
      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
   
      var byteArray = new Uint8Array(byteNumbers);
   
      byteArrays.push(byteArray);
    }
   
    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }
  storeImage(imageName) {
    let saveObj = { img: imageName };
    this.storedImages.push(saveObj);
    this.storage.set(STORAGE_KEY, this.storedImages).then(() => {
      setTimeout(() =>  {
        alert(imageName)
      }, 500);
    });
  }

}
