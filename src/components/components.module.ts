import { NgModule } from '@angular/core';
import { Draw } from './draw/draw';
import { SignComponent } from './sign/sign';
@NgModule({
	declarations: [Draw,
    SignComponent],
	imports: [],
	exports: [Draw,
    SignComponent]
})
export class ComponentsModule {}
